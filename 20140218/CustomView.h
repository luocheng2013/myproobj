//
//  CustomView.h
//  20140218
//
//  Created by wangxin mao on 14-2-19.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomView : UIView
{
    UIButton *tempBtn;
    id _target;
    SEL _action;

}

- (id)initWithTarget:(id)target action:(SEL)action;

@property (nonatomic, retain) NSString *myStr;
@end
