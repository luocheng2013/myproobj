//
//  CustomView.m
//  20140218
//
//  Created by wangxin mao on 14-2-19.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

@synthesize myStr;

- (id)initWithTarget:(id)target action:(SEL)action
{
    self = [super init];
    if (self) {
        
        _target = target;
        _action = action;
        
        tempBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [tempBtn setFrame:CGRectMake(20, 20, 40, 40)];
        [tempBtn setTitle:@"tempBtn" forState:0];
        [tempBtn addTarget:self action:@selector(pressTempBtn) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:tempBtn];
    }
    return self;
}


- (void)pressTempBtn
{
    self.backgroundColor = [UIColor yellowColor];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [_target performSelector:_action withObject:self];
    NSLog(@"%s",__FUNCTION__);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
