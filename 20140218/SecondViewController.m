//
//  SecondViewController.m
//  20140218
//
//  Created by wangxin mao on 14-2-18.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "SecondViewController.h"
#import "CustomTextView.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    CustomTextView *myTextView = [[CustomTextView alloc] initWithFrame:CGRectMake(20, 50, 200, 200)];
    myTextView.backgroundColor = [UIColor grayColor];

    [self.view addSubview:myTextView];

}

//- (BOOL)canBecomeFirstResponder
//{
//    return YES;
//}
//
//- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
//{
//    if (action == @selector(copy:)) {
//        return YES;
//    } else {
//        return NO;
//    }
//    
//}
//
//- (void)copy:(id)sender
//{
//    NSLog(@"%s",__FUNCTION__);
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
