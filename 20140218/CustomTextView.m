//
//  CustomTextView.m
//  20140218
//
//  Created by wangxin mao on 14-3-13.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "CustomTextView.h"



@implementation CustomTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(copy:)) {
        return YES;
    } else {
        return NO;
    }

}

- (void)copy:(id)sender
{
    NSLog(@"%s",__FUNCTION__);

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
