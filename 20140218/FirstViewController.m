//
//  FirstViewController.m
//  20140218
//
//  Created by wangxin mao on 14-2-18.
//  Copyright (c) 2014年 wangxin mao. All rights reserved.
//

#import "FirstViewController.h"
#import "CustomView.h"
#import "MyViewController.h"
#import "MLNavigationController.h"

@interface FirstViewController ()
{
    UIImageView *tempView;
    CustomView *myCustomView;
    CGPoint beginPoint;
    UIButton *tempBtn;
    UILabel *testLab;
}
@end

@implementation FirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
    
    
    tempBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [tempBtn setFrame:CGRectMake(70, 50, 80, 80)];
    [tempBtn setTitle:@"push" forState:0];
    [tempBtn addTarget:self action:@selector(pressTempBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:tempBtn];
    
    
    testLab = [[UILabel alloc] initWithFrame:CGRectMake(100, 30, 160, 20)];
    testLab.backgroundColor = [UIColor redColor];
    testLab.textColor = [UIColor blueColor];
    testLab.text = @"UILabel";
    [self.view addSubview:testLab];

    

    myCustomView = [[CustomView alloc] initWithTarget:self action:@selector(changeCoclor:)];
    [myCustomView setFrame:CGRectMake(80, 80, 80, 80)];
    [myCustomView setBackgroundColor:[UIColor greenColor]];
    [self.view addSubview:myCustomView];
    
    
//    [myCustomView addObserver:self forKeyPath:@"backgroundColor" options:0 context:nil];
    
//    UIPanGestureRecognizer *panGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(goAnimation:)];
//    [tempView addGestureRecognizer:panGes];
        
}


- (void)changeCoclor:(CustomView *)receiveView
{
    receiveView.backgroundColor = [UIColor brownColor];
}


- (void)pressTempBtn
{
    MyViewController *myTestVC = [[MyViewController alloc] init];
    MLNavigationController *customNav = [[MLNavigationController alloc] initWithRootViewController:myTestVC];
    
    [self.navigationController pushViewController:customNav animated:YES];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSLog(@"%s",__FUNCTION__);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *TempTouch = [touches anyObject];
    
    beginPoint = [TempTouch locationInView:tempView];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    CGPoint nowPoint = [touch locationInView:tempView];
    
    float offsetX = nowPoint.x - beginPoint.x;
    float offsetY = nowPoint.y - beginPoint.y;
    
    tempView.center = CGPointMake(tempView.center.x + offsetX, tempView.center.y + offsetY);
}

- (void)goAnimation:(id)sender
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
